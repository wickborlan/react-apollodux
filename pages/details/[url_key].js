import React from 'react';
import gql from 'graphql-tag';
import { withApollo } from '../../lib/apollo';
import { useQuery } from '@apollo/react-hooks';
import Layout from '../../components/layout';
import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useRouter } from 'next/router';
import { compose } from 'redux';
import { withRedux } from '../../lib/redux';
import ReactHtmlParser from 'react-html-parser';
import Link from 'next/link';

const PRODUCT = gql`
    query getProduct($urlKey: String!) {
        products(filter: { url_key: { eq: $urlKey } }) {
            items {
                name
                sku
                stock_status
                description {
                    html
                }
                image {
                    url
                    label
                }
                price_range {
                    minimum_price {
                        regular_price {
                            currency
                            value
                        }
                        final_price {
                            currency
                            value
                        }
                    }
                }
            }
        }
    }
`;

const Detail = () => {
    const router = useRouter();
    const dispatch = useDispatch();
    const [qty, setQty] = useState();
    const { url_key } = router.query;
    const { loading, data } = useQuery(PRODUCT, {variables: {urlKey: url_key}})

    if(loading) {
        return <div>Loading ...</div>
    }

    const product = data.products.items[0];
    const pageConfig = {
        className: 'page-detail',
        title: url_key
    }

    const handleChangeQty = (e) => {
        setQty(e.target.value);
    }

    const handleAddToCart = (e) => {
        e.preventDefault();
        const item = {
            sku: product.sku,
            name: product.name,
            image: product.image.url,
            qty: qty,
            price: product.price_range.minimum_price.final_price,
        };

        //dispatch( addToCart(item) );
        dispatch({
            type: 'ADD_TO_CART',
            item,
        });
    }

    return (
        <Layout pageConfig={pageConfig}>
            <div className="box-image">
                <div className="detail-image">
                    <img src={product.image.url} alt={product.image.label} />
                </div>
            </div>
            <div className="box-info">
                <h1>{product.name}</h1>
                
                <div className="detail-info">
                    <div className="info-desc">
                        {ReactHtmlParser(product.description.html)}
                    </div>
                    <div className="info-sku">SKU : {product.sku}</div>
                    <div className="info-price">Price : {product.final_price}</div>
                </div>
                <div className="detail-cart">
                    <form onSubmit={handleAddToCart}>
                        <input type="number" name="qty" className="qty" placeholder="Qty" onChange={handleChangeQty} />
                        <button type="submit">Add to Cart</button>
                    </form>
                </div>
            </div>
            
        </Layout>
    );
}

export default compose(withApollo, withRedux)(Detail);
