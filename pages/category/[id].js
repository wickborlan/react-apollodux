import React from 'react';
import gql from 'graphql-tag';
import { withApollo } from '../../lib/apollo';
import { useQuery } from '@apollo/react-hooks';
import Layout from '../../components/layout';
import Link from 'next/link';
import { useRouter } from 'next/router';
import ReactHtmlParser from 'react-html-parser';

const CATEGORIES = gql`
    query getCategoryById($id : String!) {
        categoryList(filters: {ids: {eq: $id}}) {
            name
            url_key
            image_path
            description 
            products {
                items {
                    id
                    name
                    url_key
                    description {
                        html
                    }
                    small_image {
                        url
                    }
                    price_range {
                        minimum_price {
                            regular_price {
                                currency
                                value 
                            }
                            final_price {
                                currency
                                value
                            }
                        }
                    }
                }
            }
        }
    }
`;

const Category = () => {
    const router = useRouter();
    const { id } = router.query;
    const { loading, data } = useQuery(CATEGORIES, {variables: {id: id}})

    const pageConfig = {
        className: 'page-category',
    }

    if(loading) {
        return <div>loading .. </div>
    }

    const category = data.categoryList[0];

    return (
        <Layout pageConfig={pageConfig}>

            <div className="categorybox">
                <div className="boximg"><img src={category.image_path} /></div>
                <div className="boxinfo"><p>{ReactHtmlParser(category.description)}</p></div>
            </div>

            <div className="product-items">
                {category.products.items.map((item) => (
                    <div className="item-box">
                        <div className="item-image">
                            <img src={item.small_image.url} alt="" />
                        </div>
                        <div className="item-info">
                            <p>
                                <Link
                                    href="/details/[url_key]"
                                    as={`/details/${item.url_key}`}
                                >
                                <a>{item.name}</a>
                                </Link>
                            </p>
                            <p className="info-price">{item.final_price}</p>
                        </div>
                    </div>
                ))}
            </div>

        </Layout>
    );
}

export default (withApollo)(Category);
