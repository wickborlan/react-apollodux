import React from 'react';
import Layout from '../components/layout';

const About = () => {
    
    const pageConfig = {
        className: 'page-about',
        title: 'About Us',
    };

    return (
        <Layout pageConfig={pageConfig}>
            <div>
                <h2>About Us</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec non magna sit amet urna hendrerit bibendum. Aliquam convallis, quam at lacinia pellentesque, leo nunc dictum odio, eget pretium sem est placerat sapien. Morbi suscipit tempor justo. Nulla sodales lacinia porta. Nulla sodales eros arcu, vitae elementum lorem condimentum ac. Mauris pretium efficitur sem. Nunc vel nibh efficitur, suscipit mi quis, mattis magna.</p>
            </div>
        </Layout>
    );
}

export default About;
