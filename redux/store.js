import { createStore, applyMiddleware, combineReducers } from 'redux';
import reduxLogger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import redCart from './reducerCart';

export const initializeStore = () => {
    const middlewares = applyMiddleware(reduxLogger);
    const reducers = combineReducers({redCart});
    return createStore(reducers, composeWithDevTools(middlewares));
}