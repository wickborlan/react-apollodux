import Navigation from './navigation';
import Head from 'next/head';

const Layout = ({children, pageConfig}) => {
    const { title, className } = pageConfig;
    return (
        <div className={`content ${className}`}>
            <Head>
                <title>{ title }</title>
                <meta name="viewport" content="initial-scale=1.0, width=device-width" />
                <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;600;800&display=swap" rel="stylesheet" />
            </Head>
            <header>
                <Navigation />
            </header>
            <div className="content-wrapper">
                {children}
            </div>
            <footer>
                &copy;2020
            </footer>
        </div>
    );
}

export default Layout;
